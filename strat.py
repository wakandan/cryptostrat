import json
import math
import matplotlib.pyplot as plt

# filename = 'usdt_eth_01022016.json'
filename = 'usdt_btc_01112016.json'
starting_position = 0
starting_balance = 1000
initial_risk = 0.5
risk_threshold_buy = 0.3
risk_threshold_sell = 0.8


class Ticker:
    def __init__(self, popen, pclose, phigh, plow):
        self.open = popen
        self.close = pclose
        self.high = phigh
        self.low = plow

    def price(self):
        return (self.open + self.close + self.high + self.close) / 4


with open(filename, 'r') as f:
    data = json.load(f)
    print 'total number of tickers {}'.format(len(data))
    tickers = []
    risk = initial_risk
    balance = starting_balance
    position = starting_position
    risks = []
    xs = []
    for i in data:
        ticker = Ticker(i['open'], i['close'], i['high'], i['low'])
        tickers.append(ticker)
        x = ticker.close * 1.0 / ticker.open
        xs.append(x)
        print x
        if x > 1:
            risk = risk + (1 - risk) / (math.e - 1) * (math.pow(1 + 1.0 / x, x - 1) - 1)
        else:
            risk = risk / math.log(2) * math.log(1 + x)
        # buy more if risk of going up is high
        if risk <= risk_threshold_buy and balance > 0:
            # poloniex fee is 0.15% for maker
            amount = balance * 1.0 / (ticker.price() * 1.15)
            position += amount
            # reset balance for ease of calculation
            balance = 0
        elif risk >= risk_threshold_sell and position > 0:
            amount = position * ticker.price() * 0.9975
            balance += amount
            position = 0
        risks.append(risk)
    print position
    print balance
    plt.plot(xs, risks)
    plt.show()
    # if at the end we have coins left, convert into money
    if position > 0:
        balance = position * tickers[-1].price() * 0.9975

    print 'final balance = {}'.format(balance)
    print 'profit = {}%'.format(balance * 100. / starting_balance)
